# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LOCK_LR.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_LO_LR.py $

import sys
import time
import ISC_library
from guardian import GuardState, GuardStateDecorator, NodeManager
import cdsutils as cdu


##################################################
# NODES
##################################################

nodes = NodeManager(['SQZ_CLF_LR',
                     'SQZ_OPO_LR',
                     'SQZ_LO_LR',
                     ])

#SED changed the nominal state while we are not regularly using squeezing so that the guardians can be OK
#We will be using squeezing regularly now. Changed the nominal (NK)
nominal = 'SQZER_READY_HD'


##################################################
# FUNCTIONS
##################################################
def OPO_LOCKED_CLF():
    flag = False
    if ezca['GRD-SQZ_OPO_LR_STATE_S'] == 'LOCKED_CLF_DUAL':
        flag = True
    return flag

#def check_OPO_rail():
#    flag = False
#    if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
#        nodes['SQZ_OPO_L1'] = 'DOWN'
#        nodes['SQZ_CLF_L1'] = 'DOWN'
#        nodes['SQZ_LO_L1'] = 'DOWN'        
#        flag = True
#    return flag

def TTFSS_LOCKED():
    flag = False
    if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
        flag = True
    return flag

def LO_LOCKED_HD():
    flag = False
    if ezca.read('GRD-SQZ_LO_LR_OK', as_string = True) == 'True':
        flag = True
    return flag

def LO_LOCKED_OMC():
    flag = False
    if ezca.read('GRD-SQZ_LO_LR_OK', as_string = True) == 'True':
        flag = True
    return flag

class TTFSS_checker(GuardStateDecorator):
    def pre_exec(self):
        if not TTFSS_LOCKED():
            log('TTFSS unlocked')
            if ezca.read('SQZ-FIBR_LOCK_STATE', as_string=True) == 'Failed':
                ezca['SQZ-FIBR_LOCK_LOGIC_ENABLE'] = 0
                time.sleep(0.2)
                ezca['SQZ-FIBR_LOCK_LOGIC_ENABLE'] = 1
            return 'IDLE'
        else:
            return True
'''
def IMC_LOCKED():
    log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    return flag
'''

#############################################
#States
#############################################



class INIT(GuardState):
    index = 0

    def main(self):
         nodes.set_managed()
         return True


class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        nodes['SQZ_OPO_LR'] = 'DOWN'
        nodes['SQZ_CLF_LR'] = 'DOWN'
        nodes['SQZ_LO_LR'] = 'DOWN'
        pass

    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False

    @TTFSS_checker
    def run(self):
        if not TTFSS_LOCKED():
            log('Waiting for TTFSS to lock')
            return False
        if not OPO_LOCKED_CLF():
            log('Waiting for OPO to lock')
            return False
        return True


# Lock with high seed power for IFO alignment 
class LOCKING_SEED_HIGH(GuardState):
    index = 24
    request = False

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        nodes['SQZ_OPO_LR'] = 'LOCKED_SEED_HIGH'

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO_LR'].arrived and nodes['SQZ_OPO_LR'].done:
            log('OPO locked with SEED high power.')
            return True

# Lock with low seed power for NLG measurement
# Some thing didn't work here (Jan3)
class LOCKING_SEED_LOW(GuardState):
    index = 34
    request = False

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        if ezca['GRD-SQZ_OPO_LR_STATE_N'] != 37 :
            nodes['SQZ_OPO_LR'] = 'LOCKED_SEED_LOW'

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO_LR'].arrived :
            log('OPO locked with SEED low power.')
            return True


class LOCKING_CLF_HD(GuardState):
    index = 13
    request = False

    @ISC_library.unstall_nodes(nodes)
    #@TTFSS_checker
    def main(self):
        nodes['SQZ_OPO_LR'] = 'LOCKED_CLF_DUAL'
        self.counter =0

    @ISC_library.unstall_nodes(nodes)
    #@TTFSS_checker
    def run(self):
        if nodes['SQZ_OPO_LR'].arrived and self.counter == 0:
#            time.sleep(3) #allow time for PZT err correction. Feel free to replace with anything better than sleep (NK)
            nodes['SQZ_CLF_LR'] = 'LOCKED'
            nodes['SQZ_LO_LR'] = 'LOCKED_HD'
            self.counter += 1
        elif self.counter == 1:
            if nodes['SQZ_LO_LR'].arrived and nodes['SQZ_CLF_LR'].arrived:
                log('LO and CLF locked')
                return True


class PREP_IFO_SQZ(GuardState):
    index = 14

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        ezca['SQZ-ASC_WFS_SWTCH'] = 0
        time.sleep(0.1)
        nodes['SQZ_LO_LR'] = 'DOWN'
        nodes['SQZ_OPO_LR'] = 'LOCKED_CLF_DUAL'
        self.counter =0

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO_LR'].arrived and self.counter == 0:
#            time.sleep(3) #allow time for PZT err correction. Feel free to replace with anything better than sleep (NK)
            # Added a counter here to keep it from requesting LOCKED over and over and over and over
            # and over and over and over and over
            # ...
            # and over.  --TJ
            nodes['SQZ_CLF_LR'] = 'LOCKED'
            self.counter = 1
        if nodes['SQZ_CLF_LR'].arrived and self.counter == 1:
            return True


class LOCKING_3MHZ_OMC(GuardState):
    index = 15
    request = False

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        #ezca['SYS-MOTION_C_BDIV_C_OPEN'] = 1
        self.counter =0
        nodes['SQZ_LO_LR'] = 'LOCKED_OMC'
        self.counter +=1

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if self.counter ==1:
            if nodes['SQZ_LO_LR'].arrived and nodes['SQZ_CLF_LR'].arrived:
                log('LO and CLF locked')
                return True



       
class LOCKED_SEED_HIGH(GuardState):
    index = 27

    @ISC_library.unstall_nodes(nodes)
    def run(self):
       if ezca['GRD-SQZ_OPO_LR_STATE_N'] == 27:
           return True
       else:
           return 'LOCKING_SEED_HIGH'


class LOCKED_SEED_LOW(GuardState):
    index = 37

    @ISC_library.unstall_nodes(nodes)
    def run(self):
       if ezca['GRD-SQZ_OPO_LR_STATE_N'] == 37:
           return True
       else:
           return 'LOCKING_SEED_LOW'

     
class SQZER_READY_HD(GuardState):
    index = 17

    @ISC_library.unstall_nodes(nodes)
    #@check_OPO_rail
    #@TTFSS_checker
    def run(self):
        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            nodes['SQZ_OPO_LR'] = 'DOWN'
            nodes['SQZ_CLF_LR'] = 'DOWN'
            nodes['SQZ_LO_LR'] = 'DOWN' 
            return 'LOCKING_CLF_HD'
        if ezca.read('GRD-SQZ_LO_LR_STATE_S', as_string=True) != 'LOCKED_HD':
            notify('LO not locked') #no need to do anything. LO will relock itself

        return True

class SQZER_READY_OMC(GuardState):
    index = 18

    @ISC_library.unstall_nodes(nodes)
    #@TTFSS_checker
    def run(self):
        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            nodes['SQZ_OPO_LR'] = 'DOWN'
            nodes['SQZ_CLF_LR'] = 'DOWN'
            nodes['SQZ_LO_LR'] = 'DOWN' 
            return 'PREP_IFO_SQZ'

        if ezca.read('GRD-SQZ_LO_LR_STATE_S', as_string=True) != 'LOCKED_OMC':
            notify('LO not locked')

        return True

class SQZ_ASC_ON(GuardState):
    index = 30

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        #set input matrix
        for py in ['PIT','YAW']:
            ISC_library.sqz_asc_intrix[py].put('POS',[],0) 
            ISC_library.sqz_asc_intrix[py].put('ANG',[],0)
            ISC_library.sqz_asc_intrix[py]['POS','AS_A_42'] = 1
            ISC_library.sqz_asc_intrix[py]['ANG','AS_B_42'] = 1
        for py in ['P','Y']:
            ezca.get_LIGOFilter('SQZ-ASC_POS_%s'%(py)).only_on('FM2', 'DECIMATION','OUTPUT')
            ezca.get_LIGOFilter('SQZ-ASC_ANG_%s'%(py)).only_on('FM2', 'DECIMATION','OUTPUT')
            ezca['SQZ-ASC_ANG_%s_GAIN'%(py)] = 0.1
            ezca['SQZ-ASC_POS_%s_GAIN'%(py)] = 0.1
        ezca['SQZ-ASC_WFS_SWTCH'] = 1
        self.timer['pause'] = 5
        self.counter =0        
        
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if self.timer['pause'] and self.counter == 0:
            ezca.get_LIGOFilter('SQZ-ASC_POS_P').switch_on('INPUT')
            ezca.get_LIGOFilter('SQZ-ASC_ANG_P').switch_on('INPUT')
            ezca.get_LIGOFilter('SQZ-ASC_POS_Y').switch_on('INPUT')
            ezca.get_LIGOFilter('SQZ-ASC_ANG_Y').switch_on('INPUT')
            self.counter +=1

        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            nodes['SQZ_OPO_LR'] = 'DOWN'
            nodes['SQZ_CLF_LR'] = 'DOWN'
            nodes['SQZ_LO_LR'] = 'DOWN' 
            return 'PREP_IFO_SQZ'

        if abs(ezca['SQZ-LO_SERVO_FASTMON']>9):
            log('LO rails!')
            return 'PREP_IFO_SQZ'

        if ezca.read('GRD-SQZ_LO_LR_STATE_S', as_string=True) != 'LOCKED_OMC':
            notify('LO not locked')
        return True
##################################################

edges = [
    ('INIT', 'IDLE'),
    ('DOWN', 'LOCKING_SEED_LOW'),
    ('DOWN', 'LOCKING_SEED_HIGH'),
    ('DOWN', 'LOCKING_CLF_HD'),
    ('DOWN', 'PREP_IFO_SQZ'),
    ('LOCKING_SEED_LOW', 'LOCKED_SEED_LOW'),
    ('LOCKING_SEED_HIGH', 'LOCKED_SEED_HIGH'),
    ('LOCKING_CLF_HD', 'SQZER_READY_HD'),
    ('PREP_IFO_SQZ', 'LOCKING_3MHZ_OMC'),
    ('LOCKING_3MHZ_OMC', 'SQZER_READY_OMC'),
    ('SQZER_READY_OMC','SQZ_ASC_ON'),
    ('SQZ_ASC_ON', 'PREP_IFO_SQZ'),
    ('SQZER_READY_HD', 'IDLE'),
    ('SQZER_READY_OMC', 'IDLE'),
    ('LOCKING_SEED_LOW', 'IDLE'),
    ('LOCKING_SEED_HIGH', 'IDLE'),
    ('SQZER_READY_OMC', 'PREP_IFO_SQZ'),
]


